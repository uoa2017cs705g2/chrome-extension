//When the save-settings-button is clicked.
document.getElementById("save-settings-button").addEventListener("click", () => {
    var user = {
        username: document.getElementById("username-input").value,
        password: document.getElementById("password-input").value
    }

    chrome.storage.sync.set({
        loginUser: user
    });

    console.log("Settings Saved!");
    alert("Settings saved!");
});


//When the page is finished loading the DOM
(function() {
    chrome.storage.sync.get('loginUser', (settings) => {
        var user = settings["loginUser"];
        document.getElementById("username-input").value = user.username;
        document.getElementById("password-input").value = user.password;
    });
 })();