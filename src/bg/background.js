const details = {
  WORKING_PROGRAM : "WORKING_PROGRAM",
  WORKING_FILE : "WORKING_FILE",
  SOCIAL_PLATFORM : "SOCIAL_PLATFORM"
};

const detail_ids = {
  WORKING_PROGRAM : 10,
  WORKING_FILE : 11,
  SOCIAL_PLATFORM : 12
}

//ms between each tick
const tick_interval = 1000;

//ms between status updates for most active tab.
const status_update_interval = tick_interval * 120;

//Records the ticks per tab to get most active tab over last x time period
var tab_ticks = {};

//Tick counter, cause why not?
var tick = 0;

//Pointer to the currently active tab
var current_tab = null;

//Pointer to the previous most active tabID
var previous_most_active_id = null;

//Register event for when a chrome window gets focus.
chrome.windows.onFocusChanged.addListener((windowId) => {
  //If newly focused window is not a chrome window
  if(windowId == -1){
    current_tab = null;
    return;
  }
  var window = chrome.windows.get(windowId, {populate: true}, (window) => {
    if(window.incognito) return; //Privacy, if the window is incognito, then just ignore it.
    getActiveTabForWindow(window, (err, tab) => {
      if(err) return console.error("Unable to find active tab");
      handleActiveTab(tab);
    });
  });
});

//Get the active tab of a window
function getActiveTabForWindow(window, callback) {
  for(tabIndex = 0; tabIndex < window.tabs.length; tabIndex++){
    if(window.tabs[tabIndex].active){
      return callback(null, window.tabs[tabIndex]);
    }
  }
  callback(true);
}

//Handle tab change event
chrome.tabs.onActivated.addListener((activeInfo) => {
  chrome.tabs.get(activeInfo.tabId, (tab) => {
    handleActiveTab(tab);
  });
});


//active tab has changed, deal with it.
function handleActiveTab(tab){
  current_tab = tab;
}

/**
 * Every tick_interval, will update tab_ticks with a new tick if a tab is active. (focused on screen, not chrome.tab.active)
 */
setInterval(() => {
  if(current_tab == null){
    //current_tab is unknown, so do nothing.
    //Need this check to be sure that the next check doesnt fail, have done in this format so we can possibly do other things later if we want.
    //console.log("No current tab set.");
  }
  else if(tab_ticks[current_tab.id] == undefined){
    //current tabid not in tab_ticks, so add it.
    tab_ticks[current_tab.id] = 1;
  }
  else{
    //Everything is good, so tick up!
    tab_ticks[current_tab.id] += 1;
  }
  console.log("Tick: " + tick++, tab_ticks);
}, tick_interval);

/**
 * Runs a check every status_update_interval to see if a new status needs to be distributed, if so, then status uploaded to server.
 */
setInterval(() => {
  var most_active_tab_id = findHighestActivityTabId();
  if(!most_active_tab_id){
    console.log("There is no most active tab id!");
    if(previous_most_active_id != null){
      checkIfTabStillOpen(previous_most_active_id, (stillOpen) => {
        if(!stillOpen){
          //No tabs focused this last period, and the previous most active tab is no longer open.
          //Send a generic update to the server that the user is unknown status. (basically, just remove details and set highest to "Online");
          sendStatusUpdate({
            status: "Online",
            details: []
          });

          previous_most_active_id = null;
        }
      });
    }
  }
  else{
    if(most_active_tab_id != previous_most_active_id){
      //We have a most active tab, check if the tab is still open.
      checkIfTabStillOpen(most_active_tab_id, (stillOpen, tab) => {
        if(stillOpen){
          findStatusFilter(tab, (err, statusFilter) => {
            if(err) return console.log("No status filter found || err true");
            previous_most_active_id = most_active_tab_id;
            var new_status = statusFilter.generateStatusUpdate(tab);
            sendStatusUpdate(new_status);
          });
        }
        //else -> the Tab is closed, should find the second most active. For now, just ignoring as the next update interval will catch it and send a new update anyway.
      })
    }
    else{
      console.log("Same tab is still most active, so no need for update!");
    }
    
    
  }
  //Reset the tab_ticks object.
  tab_ticks = {};

}, status_update_interval);

/**
 * Returns the tab with the highest tick count from the tab_ticks object.
 */
function findHighestActivityTabId(){
  var tabs = Object.keys(tab_ticks);
  if(tabs.length < 1) return false;
  var highest = tabs[0];
  for(i=0; i<tabs.length; i++){
      if(tab_ticks[tabs[i]] > tab_ticks[highest]){
          highest = tabs[i];
      }
  }
  return parseInt(highest);
}

function checkIfTabStillOpen(tabId, done) {
  chrome.tabs.get(tabId, (tab) => {
      //Check if error occoured.
      if (chrome.runtime.lastError) {
        console.log("Error: " + chrome.runtime.lastError.message);
        done(false);
      } else {
        done(true, tab);
      }
  });
};

function sendStatusUpdate(newStatus) {
  console.log(newStatus);
  chrome.storage.sync.get('loginUser', function(settings) {
    var user = settings["loginUser"];
    if(user.username == undefined || user.password == undefined){
      console.warn("User object not set in the settings, therefore cannot send update to server...");
      //Reset the most active tab, so that the next round will attempt to send new status again.
      most_active_tab_id = null;
      previous_most_active_id = null;
      return;
    }
    jQuery.ajax({
      type: "POST",
      url: "http://statusgauge.xyz/status",
      crossDomain: true,
      data: JSON.stringify(newStatus),
      contentType: 'application/json; charset=UTF-8',
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", "Basic " + btoa(user.username + ":" + user.password));
      },
      success: (data, status) => {
        console.log(data, status);
      },
      error: (xhr, status, err) => {
        console.log(status, err);
      }
    });
  });
  
}

// Store previous state so we can show deltas.  This is important
// because the API currently doesn't fire idle messages, and we'd
// like to keep track of last time we went idle.
var laststate = null;
var laststatetime = null;

/**
 * Checks the current state of the browser.
 */
function checkState() {
    threshold = 15;

    // Request the state based off of the user-supplied threshold.
    chrome.idle.queryState(threshold, function(state) {
        var time = new Date();
        console.log(state);
        if (laststate != state) {
          sendStatusUpdate({
              status: state,
              details: []
          });
            laststate = state;
            laststatetime = time;
        }
    });
};



document.addEventListener('DOMContentLoaded', function() {
    // Check every second (even though this is overkill - minimum idle
    // threshold is 15 seconds) so that the numbers appear to be counting up.
    checkState();
    window.setInterval(checkState, 1000);

    // Check every second (see above).
    // renderHistory();
    // window.setInterval(renderHistory, 1000);
});