/**
 * Filter object, given a regex filter, will assign this filter as the cusers current activity.
 * @param {Regex} urlFilter High fidelity regex filter to assocaite tab url with this filter
 * @param {String} genericStatus Highest level status to show for this filter (eg, Online, Offline, Invisible, Away) 
 * Note: High fedelity details are provided through a detail data collector.
 */
function StatusDetectionFilter(urlFilter, genericStatus){
    this.urlFilter = urlFilter;
    this.genericStatus = genericStatus;
    this.detailDataCollectors = {};
  
    this.setDetailDataCollector = (detailName, collectorFunction) => {
      this.detailDataCollectors[detailName] = collectorFunction;
    }
  
    this.getDetails = (tab) => {
      var details = [];
      detailNames = Object.keys(this.detailDataCollectors);
      for(detailIndex = 0; detailIndex<detailNames.length; detailIndex++){
        var detailName = detailNames[detailIndex];
        var collectedData = this.detailDataCollectors[detailName](tab);
        if(collectedData) {
          details.push({
            id: detail_ids[detailName],
            name: detailName,
            value: collectedData
          });
        }
      }
      return details;
    }
  
    this.generateStatusUpdate = (tab) => {
      return {
        status : this.genericStatus,
        details : this.getDetails(tab)
      };
    }
  }