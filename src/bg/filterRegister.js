/**
 * Handles registration & storage of filters.
 */

//Handles status filters.
var statusFilters = [];

//Find a matching status filter.
function findStatusFilter(tab, callback){
    for(sfi = 0; sfi < statusFilters.length; sfi++){
      if(tab.url.match(statusFilters[sfi].urlFilter)){
        return callback(null, statusFilters[sfi]);
      }
    }
    return callback(true);
}

function addFilter(filter){
    statusFilters.push(filter);
}