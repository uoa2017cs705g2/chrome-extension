//Generate some test filters...


var gdocsFilter = new StatusDetectionFilter("^.*(docs\.google\.com).*$", "Working");
gdocsFilter.setDetailDataCollector(details.WORKING_PROGRAM, (tab) => {
  return "Google Docs"
});
gdocsFilter.setDetailDataCollector(details.WORKING_FILE, (tab, callback) => {
  var extractedData = tab.title.match("^(.*) - Google Docs");
  if(extractedData != undefined && extractedData.length > 1) return extractedData[1];
  return null;
});

var facebookFilter = new StatusDetectionFilter("^.*(facebook\.com).*$", "Available");
facebookFilter.setDetailDataCollector(details.SOCIAL_PLATFORM, (tab) => {
  return "Facebook";
});

var messengerFilter = new StatusDetectionFilter("^.*(messenger\.com).*$", "Available");
messengerFilter.setDetailDataCollector(details.SOCIAL_PLATFORM, (tab) => {
  return "Facebook Messenger";
});



addFilter(gdocsFilter);
addFilter(facebookFilter);
addFilter(messengerFilter);