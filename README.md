# Status Gauage | Chrome Extension
Google Chrome extension that monitors the open tabs on a users web browser, this information is used to help determine a new state for the user, then this state is sent to the Status Gauge server to be distributed and analysed.

# Install & Run
1. Clone project to local directory
2. Visit `chrome://extensions` inside a chrome browser
3. Click `Load unpacked extension`, then select the directory where the project is stored.

# Credits
- Jack Haller <@aucklanduni.ac.nz>
- Ryan Tiedemann <rtie478@aucklanduni.ac.nz>
- Jay Pandya <@aucklanduni.ac.nz>
- Priyankit Singh <@aucklanduni.ac.nz>